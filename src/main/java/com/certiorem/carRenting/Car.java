package com.certiorem.carRenting;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cars")

public class Car {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "car_id")
	private String id;
	
	@Column (name = "car_brand")
	private String carBrand;
	
	@Column (name = "car_model")
	private String carModel;
	
	@Column (name = "car_door")
	private int doors;
	
	@Column (name = "car_renting_price")
	private int rentingPricePerDay;
	
	public Car() {
		
	}
	
	public Car(String carBrand, String carModel, int doors, int rentingPricePerDay) {
		this.carBrand = carBrand;
		this.carModel = carModel;
		this.doors = doors;
		this.rentingPricePerDay = rentingPricePerDay;
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public int getDoors() {
		return doors;
	}

	public void setDoors(int doors) {
		this.doors = doors;
	}

	public int getRentingPricePerDay() {
		return rentingPricePerDay;
	}

	public void setRentingPricePerDay(int rentingPricePerDay) {
		this.rentingPricePerDay = rentingPricePerDay;
	}
	
	public String printCarInfo() {
		return "Car brand: " + getCarBrand() + ", Car model: " + getCarModel() + ", Car doors: " + getDoors() + ", Price: " + getRentingPricePerDay();
	}
	
	
	
}
