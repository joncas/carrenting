package com.certiorem.carRenting;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.certiorem.dao.ObjectDAO;
import com.certiorem.hibernate.HibernateUtil;

public class carDAO implements ObjectDAO {
	
	private SessionFactory factory;

	public void createObject(Object object) {
		Transaction trns = null;
        if (object instanceof Car) {
        	 Car car = (Car) object;
             try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                 trns = (Transaction) session.beginTransaction();
                 session.save(car);
                 session.getTransaction().commit();
                 
             } catch (RuntimeException e) {
                 if (trns != null) {
                     try {
						trns.rollback();
					} catch (IllegalStateException e1) {
						e1.printStackTrace();
					}
                 }
                 e.printStackTrace();
             }
        }
       
		
	}

	public Object readObject(String objectId) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List<Car> cars = session.createQuery("FROM Car C WHERE C.id = " + objectId, Car.class).list();
            return new ArrayList<Object>(cars);
            
        }
	}

	public void updateObject(Object object) {
		Transaction trns = null;
        if (object instanceof Car) {
        	Car car = (Car) object;
            try (Session session = HibernateUtil.getSessionFactory().openSession()){
                trns = (Transaction) session.beginTransaction();
                session.update(car);
                session.getTransaction().commit();
            } catch (RuntimeException e) {
                if (trns != null) {
                    try {
    					trns.rollback();
    				} catch (IllegalStateException e1) {
    					e1.printStackTrace();
    				}
                }
                e.printStackTrace();
            }
        }
        	
	}

	public void deleteObject(Object object) {
		 	Transaction trns = null;
	        if (object instanceof Car) {
	        	Car car = (Car) object;
		        try (Session session = HibernateUtil.getSessionFactory().openSession()){
		            trns = (Transaction) session.beginTransaction();
		            session.delete(car);
		            session.getTransaction().commit();
		        } catch (RuntimeException e) {
		            if (trns != null) {
		                try {
							trns.rollback();
						} catch (IllegalStateException e1) {
							e1.printStackTrace();
						}
		            }
		            e.printStackTrace();
		        }
	        } 
		
	}

	public List<Object> getAllObjects() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List<Car> cars = session.createQuery("from Car", Car.class).list();
            return new ArrayList<Object>(cars);
           
        }
	}
	
	
	
}
