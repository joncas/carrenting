package com.certiorem.dao;

import java.util.List;

public interface ObjectDAO {
	
	public void createObject(Object object);
	
	public Object readObject(String objectId);
	
	public void updateObject(Object object);
	
	public void deleteObject(Object object);
	
	public List<Object> getAllObjects();

}
