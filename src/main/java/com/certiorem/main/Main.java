package com.certiorem.main;

import java.util.List;

import com.certiorem.carRenting.Car;
import com.certiorem.carRenting.carDAO;
import com.certiorem.dao.ObjectDAO;

public class Main {

	public static void main(String[] args) throws Exception {
		
		//SelectOnMenu menu = new SelectOnMenu();
		
		/*while(true) {
			menu.showMessage();
			menu.selectedMenuSwitcher();
		}*/
		
		ObjectDAO carDao = new carDAO();
		
		Car car = new Car("Ferrari", "LaFerrari", 3, 600);
		car.setId("4");
		//carDao.createObject(car);
		//carDao.deleteObject(car);
		carDao.updateObject(car);
		//carDao.getAllObjects();
		
		
		List <Object> cars = carDao.getAllObjects();
		Car castedCar = null;
		for (Object carObject: cars) {
			castedCar = (Car) carObject;
			System.out.println(castedCar.printCarInfo());
		}
		
		
		
	}

}
